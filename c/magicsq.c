#include <stdio.h>

int main(){
    int n, i, j, adj = 0, num = 2, sq[100][100] = {{0}};
    printf("Enter the order : ");
    scanf("%d", &n);
    if(n < 1){
        printf("Order must be >1 !\n");
        return 1;
    }
    if(n > 99){
        printf("Order must be <100 !\n");
        return 1;
    }
    i = n / 2;
    j = n - 1;
    sq[i][j] = 1;
    while(num <= n*n){
        i--;
        j++;

        if(i == -1 && j == n){
            i = 0;
            j = n - 2;
        }
        else{
            if(i == -1)
                i = n - 1;
            if(j == n)
                j = 0;
        }

        if(sq[i][j] != 0){
            j -= 3;
            i += 2;
            continue;
        }
    
        sq[i][j] = num++;
    }

    // Count the adjustment factor
    while(num > 0){
        num = num / 10;
        adj++;
    }
    adj += 2;

    for(i = 0;i < n;i++){
        for(j = 0;j < n;j++){
            printf("%*d", adj, sq[i][j]);
        }
        printf("\n");
    }

    return 0;
}
