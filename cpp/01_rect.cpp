#include <iostream>
#include <math.h>

using namespace std;

class rectangle {
  private:
	float h, w;

  public:
	rectangle() {
		h = 2;
		w = 3;
	}
	rectangle(int x, int y) {
		h = x;
		w = y;
	}
	void perimeter_rectangle(void) {
		int pr;
		pr = 2 * (h + w);
		cout << "\n the perimeter of the rectangle is " << pr;
	}
	void area_rectangle(void) {
		int ar;
		ar = (h * w);
		cout << "\n the perimeter of the rectangle is " << ar;
	}
};
class triangle {
  private:
	float a, b, c;

  public:
	triangle() {
		a = 2;
		b = 3;
		c = 4;
	}
	triangle(int x, int y, int z) {
		a = x;
		b = y;
		c = z;
	}
	void perimeter_triangle() {
		int pt;
		pt = a + b + c;
		cout << "\n the perimeter of the triangle is " << pt;
	}
	void area_triangle() {
		int at, s;
		s  = (a + b + c) / 2;
		at = sqrt(s * (s - a) * (s - b) * (s - c));
		cout << "\n the area of the triangle is " << at;
	}
};
class circle {
  private:
	float r;

  public:
	circle() {
		r = 3;
	}
	circle(int x) {
		r = x;
	}
	void perimeter_circle(void) {
		int pc;
		pc = 2 * 3.14 * r;
		cout << "\n the perimeter of the circle is " << pc;
	}

	void area_circle(void)

	{
		int ac;
		ac = 3.14 * r * r;
		cout << "\n the perimeter of the circle is " << ac;
	}
};

int main() {
	rectangle r1, r2(10, 5);
	triangle  t1, t2(20, 20, 20);
	circle    c1, c2(20);
	r1.perimeter_rectangle();
	r1.area_rectangle();
	r2.perimeter_rectangle();
	r2.area_rectangle();
	t2.perimeter_triangle();
	t2.area_triangle();
	c1.perimeter_circle();
	c1.area_circle();
	c2.perimeter_circle();
	c2.area_circle();
	return 0;
}
