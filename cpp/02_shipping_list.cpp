#include <iostream>
using namespace std;

class shipping_list {
  private:
	int item_code[10];
	int price[10];
	int index = 0;
	int item, i, found, flag = 0;

  public:
	void addition(void) {
		cout << "\n Enter Item Code : ";
		cin >> item_code[index];
		cout << "\n Enter Price : ";
		cin >> price[index];
		index++;
	}
	void deletion(void) {
		cout << "\n Enter Item Code : ";
		cin >> item;
		for(i = 0; i <= index; i++) {
			if(item == item_code[i]) {
				price[i]     = 0;
				item_code[i] = 0;
			}
		}
	}
	void display_price(void) {
		for(i = 0; i < index; i++) {
			cout << "Item Code : " << item_code[i];
			cout << "Price : " << price[i];
		}
	}
	void sum(void) {
		int s = 0;
		for(i = 0; i < index; i++) s = s + price[i];
		cout << "\n Total value : " << s;
	}
};

int main() {
	shipping_list sl;
	int           ch;
	do {
		cout << "\n Choose from the following :\n 1. Add an item\n 2. Delete "
		        "an item\n 3. Display total value\n 4. Display all items\n 5. "
		        "Quit\n\n>";
		cin >> ch;
		switch(ch) {
			case 1: sl.addition(); break;

			case 2: sl.deletion(); break;

			case 3: sl.sum(); break;

			case 4: sl.display_price(); break;

			case 5: cout << "\n Thank you!"; break;

			default: cout << "\n Enter a correct value";
		}
	} while(ch != 5);

	return 0;
}
