#include <iostream>
#include <math.h>

using namespace std;

class Point {
    private:
        int x, y;
    public:
        virtual void get() {
            cout << "Enter coordinates of the point : " << endl;
            cout << " x : ";
            cin >> x;
            cout << " y : ";
            cin >> y;
        }

        virtual void display() {
            cout << "The point is : " << x << ", " << y << endl;
        }
};

#define LENGTH(x1, y1, x2, y2) sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)))

class Line : public Point {
    private:
        int x1, y1, x2, y2;
    public:
        virtual void get() {
            cout << "Enter coordinates of the line : " << endl;
            cout << " x1 : ";
            cin >> x1;
            cout << " y1 : ";
            cin >> y1;
            cout << " x2 : ";
            cin >> x2;
            cout << " y2 : ";
            cin >> y2;
        }

        virtual void display() {
            double length = LENGTH(x1, y1, x2, y2);
            cout << "The length of the line is : " << length << endl;
        }
};

class Triangle : public Point {
    private:
        int x1, y1, x2, y2, x3, y3;
    public:
        virtual void get() {
            cout << "Enter coordinates of the triangle : " << endl;
            cout << " x1 : ";
            cin >> x1;
            cout << " y1 : ";
            cin >> y1;
            cout << " x2 : ";
            cin >> x2;
            cout << " y2 : ";
            cin >> y2;
            cout << " x3 : ";
            cin >> x3;
            cout << " y3 : ";
            cin >> y3;
        }

        virtual void display() {
            double l1 = LENGTH(x1, y1, x2, y2);
            double l2 = LENGTH(x1, y1, x3, y3);
            double l3 = LENGTH(x2, y3, x3, y3);
            cout << "Perimeter of the triangle : " << (l1 + l2 + l3) << endl;
        }
};

class Square : public Point {
    private:
        int x1, y1, x2, y2, x3, y3, x4, y4;
    public:
        virtual void get() {
            cout << "Enter coordinates of the square : " << endl;
            cout << " x1 : ";
            cin >> x1;
            cout << " y1 : ";
            cin >> y1;
            cout << " x2 : ";
            cin >> x2;
            cout << " y2 : ";
            cin >> y2;
            cout << " x3 : ";
            cin >> x3;
            cout << " y3 : ";
            cin >> y3;
            cout << " x4 : ";
            cin >> x4;
            cout << " y4 : ";
            cin >> y4;
        }

        virtual void display() {
            double l1 = LENGTH(x1, y1, x2, y2);
            double l2 = LENGTH(x2, y2, x3, y3);
            double l3 = LENGTH(x3, y3, x4, y4);
            double l4 = LENGTH(x4, y4, x1, y1);
            cout << "Perimeter of the square : " << (l1 + l2 + l3 + l4) << endl;
        }

};

int main() {
    Point p;
    p.get();
    p.display();

    Line l;
    l.get();
    l.display();

    Triangle t;
    t.get();
    t.display();

    Square s;
    s.get();
    s.display();

    return 0;
}
