#include <iostream>
using namespace std;
class Date {
  private:
	int day;
	int month;
	int year;

  public:
	Date() {
		day   = 0;
		month = 0;
		year  = 0;
	}

	void getdate() {
		cout << "Enter the date : ";
		cin >> day >> month >> year;
	}
	void displaydate() {
		cout << day << "/" << month << "/" << year << " ";
	}
};
class Time {
  private:
	int hour;
	int min;
	int sec;

  public:
	Time() {
		hour = 0;
		min  = 0;
		sec  = 0;
	}

	void gettime() {
		cout << "Enter the time : ";
		cin >> hour >> min >> sec;
	}
	void displaytime() {
		cout << hour << ":" << min << ":" << sec;
	}
};
class DateTime : public Date, public Time {

  public:
	void displaydatetime() {
		getdate();
		gettime();

		cout << "Date and time is ";

		displaydate();
		displaytime();

		cout << "\n";
	}
};
int main() {

	DateTime dt;
	dt.displaydatetime();
	return 0;
}
