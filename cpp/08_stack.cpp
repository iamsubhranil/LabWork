#include <iostream>

using namespace std;

#define MAX 100

#define OVERFLOW  0
#define UNDERFLOW 1

template <class T>
class Stack {
    public:
        Stack(int c) : count(c), top(0) {
            elements = new T[c];
        }

        void push(T item) {
            if(top == count)
                throw OVERFLOW;
            elements[top++] = item;
        }

        T pop() {
            if(top == 0)
                throw UNDERFLOW;
            return elements[--top];
        }

        void print() {
            cout << "{ ";
            if(top == 0) {
                cout << "<empty> }";
                return;
            }
            cout << elements[0];
            for(int i = 1;i < top;i++)
                cout << ", " << elements[i];
            cout << " }";
        }

        ~Stack() {
            delete elements;
        }
    private:
        int count, top;
        T *elements;
};

int main() {
    int i;
    cout << "Enter the number of elements : ";
    cin >> i;
    Stack<int> s = Stack<int>(i);
    while(1) {
        cout << "1. Push" << endl;
        cout << "2. Pop" << endl;
        cout << "3. Print" << endl;
        cout << "4. Exit" << endl;
        cout << "Choice : ";
        cin >> i;
        switch(i) {
            case 1: {
                        cout << "Element to push : ";
                        cin >> i;
                        try {
                            s.push(i);
                        } catch(int i) {
                            cout << "[Error] Stack overflow!" << endl;
                        }
                        break;
                    }
            case 2: {
                        try {
                            int j = s.pop();
                            cout << "Element popped : " << j << endl;
                        } catch(int i) {
                            cout << "[Error] Stack underflow!" << endl;
                        }
                        break;
                    }
            case 3: {
                        cout << "Elements of the stack : ";
                        s.print();
                        cout << endl;
                        break;
                    }
            case 4: {
                        return 0;
                        break;
                    }
            default: {
                        cout << "[Error] Wrong choice!" << endl;
                        break;
                     }
        }
    }
}
