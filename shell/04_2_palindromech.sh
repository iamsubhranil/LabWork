#!/bin/sh
echo -e "Enter the string : \c"
read n
len=${#n}
beg=1
mid=`expr $len / 2 + 1`
end=$len
palin=1
while [ $beg -lt $mid ]
do
    bc=`echo $n | cut -c$beg`
    ec=`echo $n | cut -c$end`
    if [ "$bc" != "$ec" ]
    then
        palin=0
        break
    fi
    beg=`expr $beg + 1`
    end=`expr $end - 1`
done
echo -e "'$n' is \c"
if [ $palin -eq 0 ]
then
    echo -e "not \c"
fi
echo "a palindrome!"
