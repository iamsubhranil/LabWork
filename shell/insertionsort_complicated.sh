#!/bin/sh
echo -e "Enter the number of elements : \c"
read n
i=0
while [ $i -lt $n ]; do
	echo -e "Element $i : \c"
	read el
	elements[$i]=$el
	i=`expr $i + 1`
done
echo "Input given : ${elements[@]}"
i=1
# 1. Take an element under consideration
# 2. Compare it with the elements before it and
#    find its preferable position in the array
# 4. Then insert it there shifting elements in
#    the middle to the right
while [ $i -lt $n ]; do
	baki=0
	val1=${elements[$i]}				# Pull the element
	while [ $baki -lt $i ]; do
		val2=${elements[$baki]}			# Pull the element to compare
		res=`echo "$val1 < $val2" | bc`		# Compare
		if [ $res -eq 1 ]; then			# If it's true, then shift and insert
			echo "$val1 < $val2"
			cpy=`expr $i - 1`
			trgt=$i
			while [ $cpy -ge $baki ]; do	# Shift loop
				echo $i $baki $cpy $trgt
				val3=${elements[$cpy]}	# Copy cpy'th element
				elements[$trgt]=$val3	# to (cpy + 1)th
				cpy=`expr $cpy - 1`
				trgt=`expr $trgt - 1`
				echo "${elements[@]}"
			done
			elements[$baki]=$val1		# Insert
			echo "${elements[@]}"
			break				# Break from the compare loop
		fi
		baki=`expr $baki + 1`
	done
	echo
	i=`expr $i + 1`
done
echo "${elements[@]}"
