#!/bin/sh
echo -e "Enter the number of elements : \c"
read n
i=0
while [ $i -lt $n ]; do
	echo -e "Element ${i} : \c"
	read ei
	elements[$i]=$ei
	i=`expr $i + 1`
done
echo -e "Enter the element to search : \c"
read key
i=1
while [ $i -le $n ]; do
	if [ ${elements[`expr $i - 1`]} -eq $key ]; then
		echo "Key ${key} found at ${i}th position!"
		exit
	fi
	i=`expr $i + 1`
done
echo "Key $key not found!"
